<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
## Migrations  
Migrations are built around [`taeluf/lildb`](https://gitlab.com/taeluf/php/lildb)'s `LilMigrations` class. Create migrations dir `migrate/`. Then create dirs like v1, v2 & create files up.sql, down.sql or up.php & down.php in each versioned dir. Migrating from 1 to 2 will execute v2/up.sql. From 3 down to 1 will execute v2/down.sql and v1/down.sql. You may also make files like v1/up-1.sql, v1/up-2.sql to execute multiple files in order.  
  
If you use `.sql` files for your migrations, then they should just be standard sql. If you use `.php` files for your migrations, then they should not be versioned. You may put additional .sql files in the migration dir that are not named up or down. In this case, your php script can load those files.  
  
Arg `$db` is available to `up.php` and `down.php` files. To make other vars available to your migration, override `public function get_migration_vars(): array` in your `\Tlf\BigDb` subclass.  
  
Example `up.php`:  
```php  
<?php  
$db->exec('article.create');  
$db->exec('author.create');  
$db->exec('tag.create');  
  
$db->exec('article.insert_sample_data');  
```  
One drawback with this approach is that `article.create`, `author.create`, etc are not versioned, so the migration may break in the future. A benefit of this approach is simplicity & only defining queries in one place. A safer approach would be to add the v1 create statements to the migration version dir.  
  
