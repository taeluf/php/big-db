<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Create BigDb ORMs   
BigDb's ORM provides CRUD, property getter/setter support, hooks for `db<->object` conversions, and hooks for crud events.  
  
Also See:  
- todo [Project Structure](/docs/ProjectStructure.md) - Where to put files, and a basic overview.  
- todo [BigDb Subclass](/docs/BigDbSubclass.md) - Subclass BigDb modify internals and share features across orms.  
- todo [Migrations](/docs/Migrations.md) - Upgrade & Downgrade your database schema  
- todo [Store Sql](/docs/StoredSql.md) - Store SQL, execute them, query for rows, query for ORMs  
  
  
## Tlf\BigOrm subclass  
You must subclass `Tlf\BigOrm` to have an object representation of database rows, and override `public function set_from_db(array $row)` and `public function get_db_row(): array`. You may want to subclass `Tlf\BigFormOrm` and also override `set_from_form(array $data, mixed $form_id = null)`.   
  
Optionally, you can setup save & delete hooks as well. You should also define properties & getters & setters.   
  
### BigOrm overrides  
- `public string $table;`: Set this to a string table name.  
- `public function set_from_db(array $row)`: Use the database row to initialize the properties  
- `public function get_db_row(): array`: Create an array of `column=>value` pairs to save to the database  
- `public function onWillSave(array $row): array`: Called during `save()` before INSERT or UPDATE is performed, and returns the correct (optionally modified) row to save. This method is to be overridden.  
- `public function onDidSave(array $row)`: Called after `save()`. This method is to be overridden.  
- `public function onWillDelete(array $row): bool`: Hook called before an item is deleted. Return `false` to stop deletion, `true` to continue  
- `public function onDidDelete(array $row)`: Hook called after an item is deleted from database.  
  
### BigFormOrm methods/Overrides  
- `public function set_from_form(array $data, mixed $form_id = null)`: Override this if you're subclassing BigFormOrm. Similar to set_from_db, but for user-supplied data.  
- `sanitize(mixed $input): mixed` - return strings with html removed, basically.  
- `slugify(string $text): string` - remove all chars except alpha, numeric, and hyphen (`-`). Convert to lowercase.  
  
### Additional BigOrm tips & methods  
- Define property getters & setters that will be called in any scope where the property itself is not accessible. For a prop `$prop`, `getProp()` and `setProp($value)` will be called, if `$prop` is not accessible. Typically, define `$prop` as `protected type $prop`, so it is defined, but getter/setter is called when `$item->prop` is accessed from outside the class. Magic getters/setters don't work on public properties.  
- `bin_to_uuid(string $uuid): string` converts a mysql binary uuid to a string uuid  
- `uuid_to_bin(string $uuid): string` converts a mysql string uuid to a mysql-compatible binary uuid  
- `save()` store the item in the database  
- `delete()` removes the item from the database  
- `refresh()` uses `$item->id` to re-load the row from the database & calls `set_from_db($row)` to refresh the object  
- `is_saved():bool` return true/false whether the item has been previously saved to the database. Does NOT check if the database row has been updated to match the item's current state  
- `table(): string` return the name of the database table this item maps to. Returns `$this->table` if table property is set.  
- Lazy/Alternate approach: (*not recommended*) Create a dynamic/generic, re-usable subclass of BigOrm, then override `BigDb::row_to_orm(string $table, array $row): \Tlf\BigOrm`. With this, every row could be under a single dynamic Orm class, and you wouldn't have to write an Orm for every table.   
  
### BigOrm Example  
For a fully functional (*& slightly different*) example see [test/input/BigOrm/Article.php](/test/input/BigOrm/Article.php) and the test class at [test/run/BigOrm.php](/test/run/BigOrm.php).  
```php  
<?php  
  
class Article extends \Tlf\BigFormOrm {  
  
    public int $id;  
    public string $title;  
    public string $body;  
    public string $uuid;  
    public \DateTime $createdAt;  
    public \DateTime $updatedAt;  
    public Status $status;  
    public string $slug;  
  
  
    protected int $author_id;  
    // author & url are both protected so that getAuthor() & getUrl() work. The database stores `slug` and `author_id`  
    protected Author $author;  
    protected string $url;  
  
  
    public function set_from_db(array $row){  
        // each property is mapped from a column. created_at & updated_at are converted to DateTime objects & uuid is converted from binary to string  
    }  
  
    public function set_from_form(array $data, mixed $form_id = null){  
        // similar to set_from_db, except we're coercing (& sanitizing) user-submitted data.   
    }  
  
    public function get_db_row(): array {  
        // each property is mapped to a database column. created_at is converted to a mysql-friendly string, uuid converted to binary, and so-on.  
    }  
  
    public function setAuthor(Author $author){   
        $this->author = $author;  
    }  
  
    public function getAuthor(): Author {  
        // lazy-load $this->author by using $this->author_id, and return $this->author  
        if (isset($this->author))return $this->author;  
        return $this->author = $this->db->get('author', ['id'=>$this->author_id])[0];  
    }  
  
    public function getUrl(): string {  
        return '/article/'.$this->slug.'/';  
    }  
  
    public function onWillSave(array $row): array {  
        // return the row that should actually be saved  
    }  
  
    public function onDidSave(array $row) {  
        $this->refresh(); // reloads the row from the database, which populates anything generated mysql-side (like datetime columns & uuid)  
    }  
  
    public function onWillDelete(array $row): bool {  
        return true; // return false to stop deletion  
    }  
  
    public function onDidDelete(array $row) {  
        // cleanup, such as deleting article body if it is stored on disk & not in the database  
    }  
}  
```  
