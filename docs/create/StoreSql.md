<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
  
## SQL  
The SQL files are entirely built around [`taeluf/lildb`](https://gitlab.com/taeluf/php/lildb)'s `LilSql` class.  
  
In the `sql` dir, you'll define files like `article.sql`, `author.sql`, and others. Typically, one for each table, but the file/key structure is up to you.  
  
You'll define a key, optional stop-string (default is `;`), and then the query (followed by the stop-string)  
  
article.sql:  
```sql  
-- @query(create, -- END)  
DROP TABLE IF EXISTS `article`;  
CREATE TABLE IF NOT EXISTS `article` (  
    `id` int(11) PRIMARY KEY,  
    `title` varchar(256),  
    `description` TEXT,  
    `slug` varchar(256),  
    `created_at` datetime,   
    `status` varchar(30),  
    `related_article_id` int(11)  
) ;   
-- END  
  
-- @query(get_public)  
SELECT * FROM `article` WHERE `status` LIKE 'public';  
  
-- @query(pseudo_binds)  
SELECT * FROM `article` WHERE `status` LIKE :status ${and_where};  
```  
  
Above you see the line `-- @query(create, -- END)`. After, you see a `DROP TABLE` statement and a `CREATE TABLE` statement. Then the line `-- END`. Since the file is `article.sql`, this (multi) query can be accessed via `article.create` through `$bigdb->exec('article', 'create')`.   
  
Alternatively, you can provide the full key `article.create` via `$bigdb->exec('article', 'article.create')`. This is more useful if you have a `utility.sql` file that queries from the article table. In that case, you might use `$bigdb->query('article', 'utility.get_all_articles')`.  
  
`$bigdb->query('article', 'get_public')` will return an array of article orms where the status is `public`.  
  
Additionally, these queries can have pseudo-bindable paramaters in two formats. A `:some_key` in your query will be `str_replace`d by a `PDO::quote` on the value. A `{$some_key}` will be replaced literally (no quote).   
