<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
BigDb is a database layer designed for extension, such that developers can build easy-to-use database libraries. Those libraries contain Orms, migrations, and stored queries. If you want to DEVELOP one of those libraries, this is the right place to be.   
  
If you want to USE one of those libraries, see [Documentation for end users](/docs/EndUsers.md).  
  
## Introduction  
The standard setup involves subclassing `Tlf\BigDb`, specifying a namespace to load BigOrm subclasses from, and a fixed directory structure. However, you can hack together your whole own setup if you prefer. For a hacky-own-solution, review the [Source Code](/src/) and [Tests](/test/run/). Any *must*s below are for the standard setup.  
  
For any of your subclasses, look for methods with the `@override` tag in the docblock. Try `grep -R @override` to find such methods.  
  
  
  
  
  
  
TODO: Write these docs  
  
  
  
  
  
## Directory structure  
This is a sample directory structure for a BigDb library.   
```  
- MyDb.php # Subclass of Tlf\BigDb, and must be at the root of your database library.  
- orm/ # Tlf\BigOrm subclasses SHOULD go in here, but don't have to - that's up to your autoloader.  
    - Article.php  
    - Author.php  
- sql/ # Stored queries must go here  
    - article.sql # sql queries for the table `article`  
    - author.sql # queries for `author` table.  
- migrate/  
    - v1/ # Your initial database setup. You can use .sql OR .php.   
        up.sql # a raw .sql file for migration.   
        down.php # optional. Reverse migrations aren't always needed  
    - v2/ # Second version of your database. v3 is 3rd, and so-on  
        ...   
```  
