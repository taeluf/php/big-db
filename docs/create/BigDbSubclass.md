<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Subclass BigDb  
BigDb is a database layer designed for extension, such that developers can build easy-to-use database libraries. Those libraries contain Orms, migrations, and stored queries.   
  
## Also See  
- todo [Project Structure](/docs/ProjectStructure.md) - Where to put files, and a basic overview.  
- todo [BigDb Subclass](/docs/BigDbSubclass.md) - Subclass BigDb modify internals and share features across orms.  
- todo [Create ORMs](/docs/OrmCreation.md) - Represent database rows with php objects  
- todo [Migrations](/docs/Migrations.md) - Upgrade & Downgrade your database schema  
- todo [Store Sql](/docs/StoredSql.md) - Store SQL, execute them, query for rows, query for ORMs  
  
You MUST subclass BigDb and define the namespace, or override the relevant methods. With this standard setup, a table like `article` MUST map to `Tlf\BigDb\Test\Orm\Article`. (*simply `ucfirst($table_name)`*)  
```php  
<?php  
namespace Tlf\BigDb\Test;  
  
class ArticlesDb extends \Tlf\BigDb {  
  
      
    protected string $orm_namespace = 'Tlf\\BigDb\\Test\\Orm';  
    // You can make this public if you want it changeable in code  
    protected string $db_name = 'tlf_articles';  
  
    /**  
    * Return the root directory that contains `orm`, `sql`, and `migrate` dirs.   
    * Overriding is optional  
    * Defaults to the directory this subclass is defined in.   
    */  
    public function get_root_dir(): ?string {   
        return __DIR__.'/db/';  
    }  
}  
```  
  
*Tip: Override `public function get_orm_namespace(): string` or `public function get_orm_class(string $table): string`*  
  
### Common BigDb methods to overridde  
- `public function get_root_dir(): ?string` - Override this if your BigDb subclass is NOT at the root dir of your library.  
- `public function get_orm_class(string $table): string` - Override if the `namespace` + `ucfirst($table_name)` mappings will not work for you.  
- `public function get_migration_dir(): string` - Override if your migration dir is not at your `root_dir/migrate/`  
- `public function get_migration_vars(): array` - Override to make additional vars available to your library's migrations. Default passes `['db'=>$this]`, so `up.php` and `down.php` scripts have access to the BigDb instance via the variable `$db`.  
  
### Uncommon BigDb methods to override  
- `public function migrate(int $version_from, int $version_to)` - Override to use a different migration scheme than LilDb's LilMigrations class  
- `public function init_sql()` - override to use a different sql storage system than LilDb's LilSql. If you do this, set statements to `$this->sql[$query_key] = 'SOME SQL STATEMENT';`, and you would probably want to override `recompile_sql()` as well.  
- `insert(...)`, update, delete, select - You can override these if you don't want to use LilDb as a backend.  
- `public function row_to_orm(string $table, array $row): \Tlf\BigOrm `  
