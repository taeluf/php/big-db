<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/BigDbCli.php  
  
# class Tlf\BigDb\BigDbCli  
BigDb's own CLI, for setting up BigDb libraries.  
See source code at [/src/BigDbCli.php](/src/BigDbCli.php)  
  
## Constants  
  
## Properties  
- `protected array $config = [];`   
- `protected \PDO $pdo;`   
  
## Methods   
- `public function __construct()`   
- `public function init()` add commands to cli  
- `public function cmd_create_bin(array $args, array $named_args)` Setup config / database credentials  
  
- `public function get_app_dir(): string` Get path to app dir, relative to the librarys root dir  
  
- `public function get_app_class(): string`   
- `public function get_bin_file_path(): string`   
- `public function get_config_file_path(): string`   
- `public function call(\Tlf\BigDb\BigDbCli $cli, array $args)` Call a cli command  
- `public function cmd_setup(\Tlf\BigDb\Cli $cli, array $args)` Setup config / database credentials  
  
  
