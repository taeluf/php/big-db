<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/BigFormOrm.php  
  
# class Tlf\BigFormOrm  
Extend this class (instead of BigOrm) to handle user-supplied form data. This extends from BigOrm & provides additional convenience methods & a hook for setting ORM data from form.  
See source code at [/src/BigFormOrm.php](/src/BigFormOrm.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function sanitize(mixed $input): mixed` Sanitize user input by removing all html if the input is non-numeric  
  
- `public function slugify(string $text): string` Turn a string into a slug (for URLs. No slimey critters involved.)  
  
- `public function set_from_form(array $data, mixed $form_id = null)` Initialize the Orm object from a user-submitted form  
  
  
