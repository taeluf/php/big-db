<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/BigDbServer.php  
  
# class Tlf\BigDbServer  
A utility class for working with multiple BigDb databases.   
See source code at [/src/BigDbServer.php](/src/BigDbServer.php)  
  
## Constants  
  
## Properties  
- `protected array $databases = [];` Databases added to this server.  
  
  
## Methods   
- `public function addDatabase(\Tlf\BigDb $db, string $db_name = null)` Add a BigDb database to the server.  
  
- `public function __get(string $prop): \Tlf\BigDb` Get a database  
- `public function dump()` Print information about the server - currently just the databases added.  
  
