<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/BigOrm.php  
  
# class Tlf\BigOrm  
Extend this class to represent a single row from your database. Provides many convenience features like saving, hooks, and coercing values between the database version & application version  
See source code at [/src/BigOrm.php](/src/BigOrm.php)  
  
## Constants  
  
## Properties  
- `static protected $__reflection_types__ = [];` Used to track types of properties for database coersion purposes  
- `public string $table;`   
- `protected BigDb $db;`   
  
## Methods   
- `public function __construct(BigDb $db)` Create a BigOrm instance.   
- `public function get_db_row(): array` Get a db-representation of your item. This is intended to convert your Orm object into a mysql-storeable array. It is NOT intended to load from the database.  
  
- `protected function row_to_props(array $db_row, ...$props_to_coerce): void` Set each key/value pair to the same-named properties on this class  
  
- `protected function props_to_row(array $raw_props, array $coerce_props): array` Get a database-friendly row from an array of property values  
  
- `public function get_prop_value(string $prop_name, mixed $db_value): mixed` Get the database value from a property  
  
- `public function get_db_value(string $prop_name): mixed` Get the database value from a property.  
  
- `public function set_from_db(array $row)` Initialize the Orm object from a database row  
  
- `public function props_to_array(...$properties): array` Get an array of this object's properties.   
  
Your subclass may call this from `get_db_row()`  
  
- `public function set_props_from_array(array $row, ...$properties): void` Set properties from an array. Your subclass may call this from `set_from_db()`  
  
- `public function bin_to_uuid(string $uuid): string` Convert binary uuid to a string uuid (mysql compatible).   
- `public function uuid_to_bin(string $uuid): string` Convert a string uuid to a binary uuid (mysql compatible).  
- `public function generate_uuid(): string` Generate a (hopefully) mysql-compatible UUID string. Method not thoroughly tested for mysql compatability.  
  
- `public function str_to_datetime(string $mysql_datetime): \DateTime` Convert a mysql-stored datetime string to a PHP DateTime instance  
- `public function datetime_to_str(\DateTime $datetime): string` Convert a PHP DateTime object into a mysql DATETIME string  
- `public function __get(string $prop): mixed` Call and return the property getter. For `$prop = 'author'`, call `$this->getAuthor()`  
  
- `public function __set(string $prop, mixed $value)` Call the property setter. For `$prop = 'author'`, call `$this->setAuthor($value)`  
  
- `public function save(): int` Store the item in the database. If `is_saved()` returns `true`, then use an UPDATE, else use an INSERT.   
UPDATEs are performed based on the `int $id` property of the Orm object, assuming an `id int PRIMARY KEY AUTO_INCREMENT` db column.  
  
- `public function delete(): bool` Delete this item from the database, where the db column `id` matches this item's property `id`  
  
- `public function refresh(): array` Refreshes this item, so it matches what's in the database. Just queries for this item's row (by id), then calls `$this->set_from_db($row)`.  
  
- `public function is_saved(): bool` Check if the current item is already stored in the database. Default implementation returns true if `id` property isset & is > 0  
  
- `public function table(): string` Get the table name. Default implementation return `$this->table` or the lowercase version of the class name if `$this->table` is null  
  
  
