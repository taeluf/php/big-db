<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/OrmTester.php  
  
# class Tlf\BigDb\OrmTester  
Base class for testing:  
  
- Create, set props directly, and save  
- Create Orm, set props from db, modify, save  
- Delete a saved article  
- Create, set props from form, and save  
- Fail when setting from form with invalid Data  
- Fail when setting from form without appropriate access level.  
- Phad form can be loaded & printed  
- Phad view can be loaded & printed  
- Cannot be read if access is forbidden (such as articles that are private)  
- Search-related entries are created/deleted/etc  
- Cannot be created when invalid state exists (i.e. can't CREATE author page for a user who already has one, but CAN modify existing author entry)  
- Data validation via phad form (crud should inadvertently test this, but we should also make this an explicit test)  
See source code at [/src/OrmTester.php](/src/OrmTester.php)  
  
## Constants  
  
## Properties  
- `protected array $active_test = [];` Active test information  
TODO: Make this into a struct (value object bc PHP doesn't have structs!!!)  
- `public string $orm_class;`   
- `public array $create_before_test = [];` Rows to insert before running this class's tests. Each table will be emptied before inserts  
array<string table_name, array meta_data>  
meta_data array <columns: array of strings, rows: array of arrays>  
columns array<int index, string col_name> array of column names  
rows array<int row_num, array row> array of rows (row_num is not used)  
row array<int index, mixed col_value>   
- `public array $valid_form_data;`   
- `public array $invalid_form_data;`   
- `public array $call_after_dbload;` array<int index, string method_name> Methods to call after Orm is initialized from database.   
These should initialize non-database properties like an article body that is stored on disk and lazy-loaded.  
  
## Methods   
- `public function prepare()`   
- `protected function create_before_test(array $table_rows_to_insert)`   
- `public function get_pdo(): \PDO`   
- `public function get_orm(\PDO $pdo, \Tlf\BigDb $db): \Tlf\BigOrm`   
- `public function get_random_data(array $data_options, array $data_indices): array` Get a random set of data from an array of data-options.  
  
  
- `public function get_data_entries(array $data_options, array $data_indices)`   
- `public function get_data_options_from_dataset_string(array $data_options, string $dataset_indices): array`   
- `public function run_valid_data_tests(array $valid_data_options, int $default_num_datasets_to_test, callable $test_function)` Run tests on a semi-random set of data, of the given size, built from $valid_data_options, and tested by $test_function.  
Outputs useful error information  
  
CLI Option -num_tests will overwrite $default_num_datasets_to_test  
CLI Option -data "..." will cause only one test to run, for the given dataset only  
When you run tests, failed datasets will print their -data option.   
  
The passed $test_function should call `$this->pass_if(statement_that_should_be_true, "Message Describing Test")`  
  
  
