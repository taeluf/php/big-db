<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/LibraryCli.php  
  
# class Tlf\BigDb\LibraryCli  
Extend this class to provide a CLI for your BigDb library.  
See source code at [/src/LibraryCli.php](/src/LibraryCli.php)  
  
## Constants  
  
## Properties  
- `protected array $config = [];`   
- `protected \PDO $pdo;`   
  
## Methods   
- `public function __construct()`   
- `public function init()` add commands to cli  
- `public function getPdo(): \PDO` Get a PDO instance from stored config. Does not load config or perform any checks.  
  
- `public function getBigDb(): \Tlf\BigDb` Get a BigDb instance from stored config. Does not load config or perform any checks.  
  
- `public function cmd_list(array $args, array $named_args)` List available queries  
  
- `public function cmd_print(array $args, array $named_args)` Print an sql statement.  
  
- `public function cmd_exec(array $args, array $named_args)` Execute an sql statement & print number of rows affected.  
  
- `public function cmd_query(array $args, array $named_args)` Query via stored SQL statement and print results.  
  
- `public function cmd_sql(array $args, array $named_args)` Run a raw SQL query on the database  
  
- `public function cmd_explain(array $args, array $named_args)` run an EXPLAIN on a stored query.  
  
- `public function cmd_migrate(array $args, array $named_args)` Migrate from old version to target version  
  
- `public function cmd_list_migrations(array $rgs, array $named_args)` Show available migration versions  
  
- `public function cmd_recompile(array $args, array $named_args)` Recompile sql files  
  
- `public function call(\Tlf\BigDb\LibraryCli $cli, array $args)` Call a cli command  
- `public function setup(\Tlf\BigDb\Cli $cli, array $args)` Setup config / database credentials  
  
  
