<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Defaults.php  
  
# class Tlf\BigDb\Defaults  
Provides default configuration constants  
See source code at [/src/Defaults.php](/src/Defaults.php)  
  
## Constants  
- `const CONFIG_FILE_LOCATION = "config/bigdb.json";`   
- `const APP_CLASS = "\Tlf\BigDb";`   
  
## Properties  
  
## Methods   
  
