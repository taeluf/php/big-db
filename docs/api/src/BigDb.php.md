<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/BigDb.php  
  
# class Tlf\BigDb  
Extend this class to create an interface for your BigDb library. Provides many convenience methods for various features. Uses 4 traits.   
See source code at [/src/BigDb.php](/src/BigDb.php)  
  
## Constants  
  
## Properties  
- `protected \PDO $pdo;`   
- `public \Tlf\LilDb $ldb;`   
- `protected array $sql;` Sql queries, typically from .sql files on disk  
- `protected string $orm_namespace;` The namespace from which Orm classes should be loaded  
- `protected string $db_name;` The name used by BigDbServer to identify this BigDb instance. Does NOT correspond to a mysql database. Also, {}@see get_db_name()}  
- `public string $root_dir;`   
- `public string $timezone = 'UTC';` The application's timezone as a DateTimeZone-compatible string, like 'America/Chicago' or 'UTC'  
  
All DateTimes are converted to UTC for database storage bc MySql does not store timezone information.  
  
## Methods   
- `public function __construct(\PDO $pdo)`   
- `public function getSql(): array` Get the stored sql queries  
- `public function init()` Initialize BigDb by loading queries, setting up Orm loader, and such, using a defined directory structure.   
Does nothing on `Tlf\BigDb`, but subclasse init from the directory they are defined in.  
  
- `public function get_root_dir(): string` Get path to the root of a BigDb library.   
  
- `public function init_from_dir(string $dir)` Initialize a BigDb library from a directory using the strictly defined directory structure.  
  
- `public function get_db_name(): string` Get a name for the BigDbServer to reference.  
  
