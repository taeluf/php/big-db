<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/src/OrmTesterDb/OrmTesterDb.php  
  
# class Tlf\BigDb\Test\OrmTesterDb  
  
See source code at [/test/src/OrmTesterDb/OrmTesterDb.php](/test/src/OrmTesterDb/OrmTesterDb.php)  
  
## Constants  
  
## Properties  
- `protected string $orm_namespace = 'Tlf\\BigDb\\Test\\OrmTesterOrm';`   
- `protected string $db_name = 'ormtester';`   
  
## Methods   
  
