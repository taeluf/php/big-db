<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/BigDbTasks/orm/Task.php  
  
# class Tlf\BigDb\Test\Orm\Task  
  
  
## Constants  
  
## Properties  
- `public int $id;`   
- `public string $title;`   
- `public string $due_date;`   
- `public string $date_completed;`   
- `public string $uuid;`   
- `public string $created_at;`   
- `public string $updated_at;`   
  
## Methods   
- `public function set_from_db(array $row)`   
  
