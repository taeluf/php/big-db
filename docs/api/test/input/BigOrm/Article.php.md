<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/BigOrm/Article.php  
  
# class Tlf\BigDb\Test\Article  
  
  
## Constants  
  
## Properties  
- `public bool $is_deleted = false;` `true` ONLY after a deletion. Really just for testing.  
- `public bool $checked_will_delete = false;` `true` after onWillDelete is called. Really just for testing.  
- `public int $id;`   
- `public string $title;`   
- `public string $body;`   
- `public string $uuid;`   
- `public \DateTime $createdAt;`   
- `public \DateTime $updatedAt;`   
- `protected Author $author;`   
- `public Status $status;`   
- `public string $url;`   
- `private int $author_id;`   
- `protected string $slug;`   
  
## Methods   
- `public function set_from_db(array $row)`   
- `public function set_from_form(array $data, mixed $form_id = null)`   
- `public function get_db_row(): array`   
- `public function setAuthor(Author $author)`   
- `public function getAuthor(): Author`   
- `public function setSlug(string $slug)`   
- `public function onWillSave(array $row): array` Generates a UUID if not set on the row. Sets `$this->uuid`  
  
- `public function onDidSave(array $row)` Refresh the object, to populate createdAt and updatedAt if they are not already set, since they are generated in the database.  
- `public function onWillDelete(array $row): bool` Set `$this->checked_will_delete = true`  
- `public function onDidDelete(array $row)` Set `$this->is_deleted = true`  
  
