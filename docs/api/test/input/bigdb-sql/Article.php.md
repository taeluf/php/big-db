<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/bigdb-sql/Article.php  
  
# class Tlf\LilDb\Test\BigDbOrm\Article  
  
  
## Constants  
  
## Properties  
- `protected Article $_related_article = null;` Magic property. Use $article->related_article;  
- `public int $related_article_id = null;`   
- `protected array $relating_articles = null;`   
  
## Methods   
- `public function getRelatingArticles()`   
- `public function getRelatedTitle()`   
  
