<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/BigDbApp/ArticleDb.php  
  
# class Tlf\BigDb\Test\ArticlesDb  
  
See source code at [/test/input/BigDbApp/ArticleDb.php](/test/input/BigDbApp/ArticleDb.php)  
  
## Constants  
  
## Properties  
- `protected string $orm_namespace = 'Tlf\\BigDb\\Test\\Orm';`   
- `protected string $db_name = 'articles';`   
  
## Methods   
  
