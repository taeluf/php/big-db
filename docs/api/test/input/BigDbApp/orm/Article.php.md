<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/BigDbApp/orm/Article.php  
  
# class Tlf\BigDb\Test\Orm\Article  
  
See source code at [/test/input/BigDbApp/orm/Article.php](/test/input/BigDbApp/orm/Article.php)  
  
## Constants  
  
## Properties  
- `public int $id;`   
- `public string $title;`   
- `public string $description;`   
- `public string $slug;`   
- `public string $created_at;`   
- `public string $status;`   
- `public int $related_article_id;`   
  
## Methods   
- `public function set_from_db(array $row)`   
  
