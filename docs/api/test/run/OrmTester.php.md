<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/OrmTester.php  
  
# class Tlf\BigDb\Test\OrmTester  
  
  
## Constants  
  
## Properties  
- `protected \PDO $pdo;`   
- `protected \Tlf\BigDb\Test\OrmTesterDb $db;`   
- `public string $orm_class = '\\Tlf\\BigDb\\Test\\OrmTesterOrm';`   
- `public array $valid_form_data = [  
          
    ];`   
- `public array $create_before_test = [  
        'person' =>[  
            'columns'=> ['id', 'name', 'date_of_birth'],  
            'rows'=> [  
                [1, "Reed", "2000-01-01"],  
                [2, "Bobbi", "1999-07-16"],  
                [3, "Jeffrey", "1996-05-19"],  
                [4, "Get out of here jim", "1037-11-27"],  
            ]  
        ],  
    ];` Initial rows to setup before tests are run. All tables listed will be emptied before inserting initial data.  
  
## Methods   
- `public function get_bigdb(\PDO $pdo): \Tlf\BigDb`   
- `public function prepare()`   
  
