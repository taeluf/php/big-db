<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/BigDb.php  
  
# class Tlf\BigDb\Test\BigDb  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `protected function get_coersion_tests(): array` Get array of tests.   
  
prop_value is coerced & coerced prop_value must be equal to declared db_value. db_value is coerced & the coerced db_value must be equal to to the declared prop_value  
  
test_info is array w/ three keys: type, prop_value, db_value  
test_info MAY contain key 'prop_name', which is used by some conversions like uuid.  
  
- `public function prepare()`   
- `public function testBenchCoersion()` Run benchmark on coersions.   
Test that 50+ coersions are completed per ms  
- `public function testCoerceValues()` Test coercing property values into database-values & visa-versa  
- `public function testQueryWithVar()`   
- `public function testQueryWithBind()`   
- `public function testQueryArticles()`   
- `public function testMain()`   
  
