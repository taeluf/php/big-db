<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/BigOrm.php  
  
# class Tlf\BigDb\Test\BigOrm  
  
  
## Constants  
  
## Properties  
- `protected \Tlf\BigDb $bigdb;`   
- `protected \PDO $pdo;`   
  
## Methods   
- `public function prepare()`   
- `public function finish()`   
- `public function create_article_table()`   
- `public function pdo()`   
- `public function db()`   
- `public function testCoersion()`   
- `public function testFromForm()`   
- `public function testDeleteHooks()`   
- `public function testSaveHooks()`   
- `public function testDeleteItem()`   
- `public function testSaveItem()`   
- `public function testConvertToDbRow()`   
- `public function testSetFromDb()` Test setting article props from a db row and using the dynamic properties  
  
