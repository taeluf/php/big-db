<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb Database Setup  
Setup one or more BigDb libraries in a php application.  
  
## Also See  
- [Cli](/docs/use/Cli.md) - work with your database from the command line  
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
- [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.  
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
  
## Setup a single database  
Instantiate with a pdo instance. Subclasses may define custom constructors.  
```php  
<?php  
$pdo = new \PDO(...);  
  
# ArticlesDb uses the default constructor  
$articles_db = new \Tlf\BigDb\Test\ArticlesDb($pdo);  
  
# TasksDb defines its own constructor  
$task_db_integration = new TasksDbIntegration();  
$tasks_db = new \Tlf\BigDb\Test\TasksDb($pdo, $task_db_integration);  
  
```  
  
## Multi-Database BigDbServer  
  
```php  
<?php  
$server = new \Tlf\BigDbServer();  
$server->addDatabase(new \Tlf\BigDb\Test\ArticlesDb($pdo), 'articles'); // 2nd param is optional & typically defined by the BigDb instance  
$server->addDatabase(new \Tlf\BigDb\Test\TasksDb($pdo));  
  
// Use `$server->db_name to get the BigDb instance you need. (uses magic __get())  
$articles = $server->articles->get('article');  
  
// display a list of databases setup with your BigDbServer.  
$server->dump();   
```  
