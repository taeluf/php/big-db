<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Run Migrations in BigDb  
Run database setup, upgrades, & downgrade.  
  
## Also See  
- [Create Migrations](/docs/create/Migrations.md) - Upgrade & Downgrade your database schema    
- [Cli](/docs/use/Cli.md) - work with your database from the command line  
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
- [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.  
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
- [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries  
  
## Migrate via CLI  
```bash  
// syntax  
vendor/bin/bigdb migrate [current_version] [target_version]  
  
// initialize database, from version zero to version one.  
vendor/bin/bigdb migrate 0 1  
  
// upgrade from version 1 to version 3  
vendor/bin/bigdb migrate 1 3  
```  
  
  
## Migrate in PHP  
Initialize the library's databse & call `$db->migrate(old_version, new_version)`  
```php  
<?php  
$pdo = new \PDO('mysql:dbname='.$db_name.';host='.$db_host, $user_name, $password);  
  
$db = new \Tlf\BigDb\Test\ArticleDb($pdo);   
  
// initialize database  
$db->migrate(0,1);   
  
// upgrade from version 1 to version 3  
$db->migrate(1,3);  
```  
