<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb ORM Usage  
ORMs can be loaded from stored queries, raw sql queries, or through convenience getters.  
  
Orms can insert, update, and delete database rows.  
  
## Also See  
- [Cli](/docs/use/Cli.md) - work with your database from the command line  
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
- [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries  
  
## BigOrm API  
- `$item->prop` maps to `$item->getProp()` and `$item->setProp()` in any scope where `$item->prop` is not accessible.  
- `$item->save()` will `INSERT` or `UPDATE` an item. On insert, the item's `id` is updated.  
- `$item->delete()` will delete the current item from the database, if it is stored. Returns `true` if a deletion occurs, `false` otherwise.  
- `$item->refresh()` reloads the item's row from the database & calls `$item->set_from_db($loaded_row)`. Throws if `$item->id` is not set, or if a row is not returned for the `$item->id`  
- `$item->is_saved()` returns true if the item is already in the database. It does NOT check if the current item has been modified and is different from the database version.  
  
*Tip: `delete()` will `unset($this->id)`, making the property inaccessible. This is a design flaw and implementation may change.*  
  
## Load Orms  
- `$db->get('table_name', $where=['status'=>'public']): array`: get an array of Orms  
- `$db->query('table_name', 'stored_query_name', $experimental_binds = []): array`: Get an array of `Orms` by executing a stored query. (*stored queries should be documented in the library you're using*)  
- `$orm = new \YourNs\OrmName($db); $orm->set_from_db(array $row)`: Instantiate from row  
- `$db->row_to_orm('table_name', array $row): Tlf\BigOrm`: intialize from user-submitted data.   
  
## BigFormOrm API  
For `BigOrms`s that expect user-submitted data.  
- `$item->set_from_form(array $post_data, mixed $form_id = null)` - intialize from user-submitted data.   
- `$item->sanitize(mixed $input): mixed` - return strings with html removed, basically. (*FormOrms may handle this for you.*)  
- `$item->slugify(string $text): string` - remove all chars except alpha, numeric, and hyphen (`-`). (*FormOrms may handle this for you.*)  
  
## SELECT ORMs  
```php  
<?php  
// orms from where conditions  
$where = ['status'=> 'public'];  
$articles = $db->get('article', $where);  
  
// orms from Stored SQL  
$articles = $db->query('article','get_articles_where_status', $where);  
  
// orms frmo database rows  
$rows = $db->select('article', $where);  
$articles = [];  
foreach ($rows as $row){  
    // shorthand 'smart' instantiation  
    $articles[] = $db->row_to_orm('article', $row);  
    // direct instantiation  
    $article = new \MyNamespace\ArticleOrm($db);  
    $article->set_from_db($row);   
    $articles[] = $article;  
}  
```  
  
## DELETE  
```php  
$article = $db->get('article', ['id'=>$int_id]);  
$did_delete = $article->delete();  
if ($did_delete)unset($article); // so you're not tempted to use it!  
```  
  
## UPDATE  
```php  
<?php  
// via orm. $articleOrm is defined in the above example  
$article = $db->get('article', ['id' => '64'])[0];  
$article->title = 'New Title through ORM';  
$article->save();  
```  
  
*Tip: `save()` updates if `id` isset, otherwise it inserts.*  
*Tip: save() invokes onWillSave() & onDidSave(), maybe generating a slug or adding itself to a search database*  
  
## INSERT  
  
**Set properties directly:**  
```php  
<?php  
  
$article = new Tlf\BigDb\Test\Article($db);  
  
$article->title = 'BigOrm has delete now!';  
$article->body = 'Saving is done, and now we have deletion!';  
$article->created_at = new \DateTime(); # normally database generated  
$article->author_id = 29;  
$article->status = \YourNS\StatusEnum::Public;  
  
$article->save(); // $article->id is set during save()  
$article->refresh(); # Loads mysql-generated columns like `uuid` and `updated_at`  
```  
  
*Tip: `$db->insert('article',$article_row = [...]);` will insert straight into db w/out the ORM layer.*  
  
**From a Database Row:**  
```php  
<?php  
  
$article = new Tlf\BigDb\Test\Article($db);  
  
$article->set_from_db(  
    $row = [   
        'title'=>'BigOrm is under development',  
        'uuid'=> $article->uuid_to_bin($article->generate_uuid()), // Normally database-generated  
        'body'=>'BigDb is a library simplifying database access. BigOrm is the lowest-level component representing a single item. BigOrm is under development. [and it would go on]',  
        'created_at' => '2023-05-12 12:05:24',  
        'updated_at' => '2023-05-12 13:15:36',  
        'author_id' => 27,  
        'status'=>'public',  
        'slug'=>'bigorm-in-development',  
    ]  
);  
$article->save(); // $article->id is set during save()  
// $article->refresh();  
```  
