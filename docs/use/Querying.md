<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb Querying  
Use BigDb's shortened syntax for simple CRUD operations and SQL execution.   
  
## Also See:  
- [Cli](/docs/use/Cli.md) - work with your database from the command line  
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
- [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.  
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
- [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries  
  
## CRUD API  
- `$db->sql_query('SELECT * FROM ...', ['status'=>'public']): array`: Get array results. Uses standard PDO prepare/bind/execute/fetchAll(FETCH_ASSOC).  
- `$db->query_rows('stored_query_name', $experimental_binds = []): array`: Get an array of rows by executing a stored query. For `query()`, you use `$table_name` + `$query_name` as separate args. For `query_rows`, you use a dot-property like `table_name.query_name`.  
- `$db->select('table_name', $where=['status'=>'public']): array`: get an array of rows matching `$where`  
- `$db->insert('table_name', $new_row = [...]): int`: Insert a new row, return the new row's primary int id  
- `$db->update('table_name', $where = ['id'=>3], $new_values = ['title'=>'New Title']): int`: Update rows matching `$where` by setting `$new_values`  
- `$db->delete('article', $where = ['id'=>3]): bool`: Delete rows matching `$where`  
  
*Tip: `$db->row_to_orm('table_name', $row)` to convert any returned rows to an orm object.*  
*Tip: `$orm->get_db_row()` to convert an orm object into a database friendly row array.*  
  
  
## ORMs API  
- `$db->get('table_name', $where=['status'=>'public']): array`: get an array of Orms  
- `$db->query('table_name', 'stored_query_name', $experimental_binds = []): array`: Get an array of `Orms` by executing a stored query. (*stored queries should be documented in the library you're using*)  
- `$db->row_to_orm('table_name', array $row): Tlf\BigOrm`: Get an Orm from an array row  
  
  
