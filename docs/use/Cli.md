<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb Command Line Interface  
BigDb's cli is used for migrations, running & recompiling stored queries, and setting up database credentials (*ALWAYS gitignore db credentials!*).  
  
## Also See  
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
- [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.  
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
- [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries  
  
**Command:** `vendor/bin/bigdb`  
**Oputput:**  
```txt  
    create-bin - Create cli script for your BigDb library.  
```  
  
**Command:** `vendor/bin/bigdb setup`:   
Creates file `config/bigdb.json`:  
```json  
{  
    "app.class": "Tlf\\BigDb",  
    "app.dir": "src\/db",  
    "mysql.host": "localhost",  
    "mysql.database": "test_db_name",  
    "mysql.user": "test_user",  
    "mysql.password": "test_password"  
}  
```  
  
*Tip: `.config/bigdb.json` is another valid location.*  
