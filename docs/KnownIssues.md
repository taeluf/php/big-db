<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb's Known Issues / Bugs  
Issues are below. Assume they won't be fixed, unless the notes say otherwise.  
  
## Will Fix  
- `bin/bigdb` cannot instantiate if the BigDb subclass's constructor is different from the parent class.  
- BigDb (via SqlVerbs) has `sql_query()` method, but needs `sql_exec()` for statements that do not return rows.  
  
## Might fix / Uncertain  
- `BigOrm::delete()` will `unset($this->id)`, so future access to `$this->id` will call `__get()`/`__set()` and generally throw an error like `Error: Call to undefined method DecaturVote\News\Orm\Author::getId()`  
    - I don't know what to do about this. I can't set `$this->id = null` bc BigOrm doesn't force id to be nullable. Leaving the `unset()` means the bug can be fixed in your script without modifying your orm. Setting null means all orms HAVE to make id nullable, and I don't want to force this. NOT unsetting or nulling the id leaves the ORM in a bad state, where it appears to be stored in the DB (bc id is set), but it actually isn't in the db.  
- the cli is currently designed for ONE bigdb library, and is really geared more toward the developer of a library than a user of a library. This needs re-thought.  
- No built-in way to add bigdb cli to the system (i.e. alias in .bashrc)  
  
## Won't Fix  
- [none yet]  
