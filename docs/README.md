<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# BigDb  
PHP Database Layer with ORM, migrations, stored sql statements, cli, and multi-database support.  
  
**NOTICE:** cli `bin/bigdb` is undergoing major changes & is currently broken.  
**NOTICE:** BigDb is mostly stable, but is still under development & much may change.  
  
## Documentation  
- Use a BigDb Library  
    - [Cli](/docs/use/Cli.md) - work with your database from the command line  
    - [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.    
    - [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.  
    - [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]`   
    - [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries  
- Create a BigDb Library  
    - todo [Project Structure](/docs/create/ProjectStructure.md) - Where to put files, and a basic overview.  
    - todo [BigDb Subclass](/docs/create/BigDbSubclass.md) - Subclass BigDb modify internals and share features across orms.  
    - todo [Create ORMs](/docs/create/OrmCreation.md) - Represent database rows with php objects  
    - todo [Migrations](/docs/create/Migrations.md) - Upgrade & Downgrade your database schema  
    - todo [Store Sql](/docs/create/StoreSql.md) - Store SQL, execute them, query for rows, query for ORMs  
- [Bugs/Issues](/docs/KnownIssues.md) - Known issues & quirks  
- Development / Other  
    - todo [Test Your ORMs](/docs/dev/Testing.md) - Simplified testing by defining data.  
    - [Changelog](/docs/Changelog.md)  
    - [Status Notes](/Status.md)  
    - [License](/LICENSE)  
    - [API Documentation](/docs/api/) - Breakdown of all the classes, their properties, and methods.   
    - [Sample BigDb Library](/test/input/BigDbApp/) - A full BigDb library used by our tests. See [BigDb Tests](/test/run/BigDb.php) for an example of how this library is used.  
  
## Install  
```bash  
composer require taeluf/big-db v1.0.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/big-db": "v1.0.x-dev"}}  
```  
  
  
## Overview | Usage  
TODO usage overview docs  
  
## Overview | Create a Database Library  
TODO library overview docs  
