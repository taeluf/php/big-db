# ChangeLog

## Other Notes
- 2023-11-27: Add EXPLAIN & raw query to cli.
- 2023-11-14: Major Documentation Overhaul (*incomplete*)

## Git Log
`git log` on @system(date, trim)`
@system(git log --pretty=format:'- %h %d %s [%cr]' --abbrev-commit)
