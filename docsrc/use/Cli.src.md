# BigDb Command Line Interface
BigDb's cli is used for migrations, running & recompiling stored queries, and setting up database credentials (*ALWAYS gitignore db credentials!*).

## Also See
- [Querying](/docs/use/Querying.md) - Simple CRUD operations and SQL execution.  
- [Use ORMs](/docs/use/OrmUsage.md) - Query for orms & use them to insert, update, and delete rows.
- [Run Migrations](/docs/use/RunMigrations.md) - `vendor/bin/bigdb migrate [cur_ver] [target_ver]` 
- [Database Setup](/docs/use/DatabaseSetup.md) - Setup one or multiple database libraries

**Command:** `vendor/bin/bigdb`
**Oputput:**
```txt
@system(bin/bigdb help, trim_empty_lines)
```

**Command:** `vendor/bin/bigdb setup`: 
Creates file `config/bigdb.json`:
```json
@file(config/sample-bigdb.json)
```

*Tip: `.config/bigdb.json` is another valid location.*
