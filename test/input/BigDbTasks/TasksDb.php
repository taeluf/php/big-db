<?php

namespace Tlf\BigDb\Test;

class TasksDb extends \Tlf\BigDb {

    protected string $orm_namespace = 'Tlf\\BigDb\\Test\\Orm';
    protected string $db_name = 'tasks';
}
