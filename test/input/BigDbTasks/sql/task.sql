-- @query(create, -- END)
DROP TABLE IF EXISTS `task`;
CREATE TABLE IF NOT EXISTS `task` (
    `id` int(11) PRIMARY KEY,
    `uuid` BINARY(16),
    `title` varchar(256),
    `due_date` datetime, 
    `date_completed` datetime, 
    `created_at` datetime, 
    `updated_at` datetime 
) ; 

DELETE FROM `task`;
-- END

-- @query(get_overdue)
SELECT * FROM `task` WHERE `due_date` < CURRENT_TIMESTAMP AND `date_completed` IS NULL;

-- @query(insert_sample_data)
INSERT INTO `task` (`id`, `title`, `due_date`, `date_completed`, `created_at`, `updated_at`)
    VALUES
    (1, 'Run', date('now','+1 month'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (2, 'Jog', date('now','-1 month'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (3, 'Walk', date('now','-2 month'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (4, 'Read', date('now','-1 day'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (5, 'Clean', date('now','+10 day'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (6, 'Get Cleaning Supplies', date('now','+10 day'), date('now', '+5 day'), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (7, 'Buy Shoes', date('now','+10 day'), date('now', '+5 day'), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP),
    (8, 'Sell House', date('now','+10 day'), NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)
;
