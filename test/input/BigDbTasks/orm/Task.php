<?php

namespace Tlf\BigDb\Test\Orm;

class Task extends \Tlf\BigFormOrm {

    public int $id;
    public string $title;
    public string $due_date;
    public string $date_completed;
    public string $uuid;
    public string $created_at;
    public string $updated_at;


    public function set_from_db(array $row){
        foreach ($row as $key=>$value){
            if ($value==null)continue;
            $this->$key = $value;
        }
    }
}
