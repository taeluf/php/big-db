<?php

namespace Tlf\LilDb\Test\BigDbOrm;

class Article extends \Tlf\BigOrm {

    /**
     * Magic property. Use $article->related_article;
     */
    protected ?Article $_related_article = null;
    public ?int $related_article_id = null;


    protected ?array $relating_articles = null;

    public function getRelatingArticles(){
        if ($this->relating_articles!==null)return $this->relating_articles;
        $this->relating_articles = $this->bdb->many('article','id',$this->id);
        return $this->relating_articles;
    }

    public function getRelatedTitle(){
        return $this->related_article->title;
    }
}
