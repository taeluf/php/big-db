<?php

namespace Tlf\BigDb\Test;

class ArticlesDb extends \Tlf\BigDb {

    protected string $orm_namespace = 'Tlf\\BigDb\\Test\\Orm';
    protected string $db_name = 'articles';
}
