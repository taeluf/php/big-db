-- @query(create, -- END) 
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
    `id` int(11) PRIMARY KEY,
    `title` varchar(256),
    `description` TEXT,
    `slug` varchar(256),
    `created_at` datetime, 
    `status` varchar(30),
    `related_article_id` int(11)
) ; 

DELETE FROM `article`;
-- END

-- @query(sample_data)
INSERT INTO `article` (`id`, `title`, `description`, `slug`, `created_at`, `status`, `related_article_id`)
    VALUES
    (1, 'One', 'Desc 1', 'one', CURRENT_TIMESTAMP, 'public', NULL),
    (2, 'Two', 'Desc 2', 'two', CURRENT_TIMESTAMP, 'public', NULL),
    (3, 'Three', 'Desc 3', 'three', CURRENT_TIMESTAMP, 'public', 1),
    (4, 'Four, Private', 'Desc 4', 'four-private', CURRENT_TIMESTAMP, 'private', 1)
;

-- @query(get_public)
SELECT * FROM `article` WHERE `status` LIKE 'public';

-- @query(get_private)
SELECT * FROM `article` WHERE `status` LIKE 'private';

-- @query(get_where)
SELECT * FROM `article` WHERE `status` LIKE :status;

-- @query(insert_with_status)
INSERT INTO `article` (`id`, `title`, `description`, `slug`, `created_at`, `status`, `related_article_id`)
    VALUES
    (5, 'Status', 'Status 1', 'status', CURRENT_TIMESTAMP, :status, NULL)
;
-- @query(get_where_clause)
SELECT * FROM `article` {$where_clause};
