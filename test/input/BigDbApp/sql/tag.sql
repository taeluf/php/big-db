-- @query(create, -- END) 
DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
    `id` int(11) PRIMARY KEY,
    `name` varchar(256),
    `description` TEXT,
    `slug` varchar(256),
    `created_at` datetime, 
    `status` varchar(30)
) ; 

DELETE FROM `tag`;
-- END

-- @query(sample_data)
INSERT INTO `tag` (`id`, `name`, `description`, `slug`, `created_at`, `status`)
    VALUES
    (1, 'One', 'Desc 1', 'one', CURRENT_TIMESTAMP, 'public'),
    (2, 'Two', 'Desc 2', 'two', CURRENT_TIMESTAMP, 'public'),
    (3, 'Three', 'Desc 3', 'three', CURRENT_TIMESTAMP, 'public'),
    (4, 'Four, Private', 'Desc 4', 'four-private', CURRENT_TIMESTAMP, 'private')
;
