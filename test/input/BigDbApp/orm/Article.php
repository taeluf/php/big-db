<?php

namespace Tlf\BigDb\Test\Orm;

class Article extends \Tlf\BigFormOrm {

    public int $id;
    public string $title;
    public string $description;
    public string $slug;
    public string $created_at;
    public string $status;
    public int $related_article_id;


    public function set_from_db(array $row){
        foreach ($row as $key=>$value){
            if ($value==null)continue;
            $this->$key = $value;
        }
    }
}
