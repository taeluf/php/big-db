<?php
/**
 * Create the tables & insert sample data
 */

$db->exec('article.create');
$db->exec('author.create');
$db->exec('tag.create');

$db->exec('article.sample_data');
