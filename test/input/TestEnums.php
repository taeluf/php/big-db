<?php

namespace Tlf\BigDb\Test\TestEnums;

enum Suit: string
{
    case Hearts = 'Hearts, YES';
    case Diamonds = 'Diamonds, YEAH';
    case Clubs = 'Clubs, WHOO';
    case Spades = 'Spades, WIN';
}
