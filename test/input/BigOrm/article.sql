-- @query(create, -- END) 
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT, -- testing on sqlite, so autoincrement, not AUTO_INCREMENT
    `title` varchar(256),
    `uuid` BINARY(16), -- testing on sqlite, so no UUID() function available
    `body` TEXT,
    `created_at` datetime DEFAULT CURRENT_TIMESTAMP, 
    `updated_at` datetime DEFAULT CURRENT_TIMESTAMP, 
    `author_id` int(11),
    `status` varchar(30),
    `slug` varchar(256)
) ; 
-- END
