<?php

namespace Tlf\BigDb\Test;

class Article extends \Tlf\BigFormOrm {

    /**
     * `true` ONLY after a deletion. Really just for testing.
     */
    public bool $is_deleted = false;
    /**
     * `true` after onWillDelete is called. Really just for testing.
     */
    public bool $checked_will_delete = false;

    public int $id;
    public string $title;
    public string $body;
    public string $uuid;
    public \DateTime $createdAt;
    public \DateTime $updatedAt;
    protected Author $author;
    public Status $status;
    public string $url;


    private int $author_id;
    protected string $slug;


    public function set_from_db(array $row){

        $this->title = $row['title'];
        $this->body = $row['body'];
        $this->uuid = $this->bin_to_uuid($row['uuid']);
        $this->createdAt = \DateTime::createFromFormat('Y-m-d H:i:s', $row['created_at']);
        $this->updatedAt = \DateTime::createFromFormat('Y-m-d H:i:s', $row['updated_at']);
        $this->status = Status::from($row['status']);
        // $this->url = '/article/'.$row['slug'].'/';
        $this->setSlug($row['slug']);

        $this->author_id = $row['author_id'];

        $this->slug = $row['slug'];
        
    }

    public function set_from_form(array $data, mixed $form_id = null){

        if (isset($data['id'])){
            $row = $this->db->select($this->table(), ['id'=>$data['id']])[0];
            $this->set_from_db($row);
        }
        
        $this->title = $this->sanitize($data['title']);
        $this->body = $this->sanitize($data['body']);
        $this->createdAt = $this->createdAt ?? new \DateTime();
        $this->updatedAt = $this->updatedAt ?? new \DateTime();
        $this->status = Status::from($data['status']);

        if (isset($data['slug']) && trim($data['slug']) != '')$this->setSlug($this->slugify($this->sanitize($data['slug'])));
        else $this->setSlug($this->slugify($this->title));

        if (is_numeric($data['author_id'])){
            $this->author_id = (int)$data['author_id'];
        }

        
    }

    public function get_db_row(): array {
        $row = [
            'title'=>$this->title,
            'body'=>$this->body,
            'author_id' => $this->author->id ?? $this->author_id,
            'status' => $this->status->value,
            'slug' => $this->slug,
        ];

        if (isset($this->createdAt)){
            $row['created_at'] = $this->createdAt->format('Y-m-d H:i:s');
        }
        if (isset($this->updatedAt)){
            $row['updated_at'] = $this->updatedAt->format('Y-m-d H:i:s');
        }


        if (isset($this->uuid) && is_string($this->uuid)){
            $row['uuid'] = $this->uuid_to_bin($this->uuid);
        }

        if (isset($this->id)){
            $row['id'] = $this->id;
        }

        return $row;
    }

    public function setAuthor(Author $author){ 
        $this->author = $author;
    }

    public function getAuthor(): Author {
        if (isset($this->author))return $this->author;
        $author = new Author($this->db);
        $author->id = $this->author_id;
        $this->author = $author;
        return $author;
    }

    public function setSlug(string $slug){
        $this->slug = $slug;
        $this->url = '/article/'.$slug.'/';
    }

    /**
     * Generates a UUID if not set on the row. Sets `$this->uuid`
     *
     * @return a row with UUID present
     */
    public function onWillSave(array $row): array {
        // Normally, mysql would generate the uuid, but I'm testing on sqlite so I'm just hardcoding this.
        $uuid = '036f9f19-9846-11ed-ba94-ac1f6bbcd39e'; 
        $row['uuid'] = $this->uuid_to_bin($uuid);
        $this->uuid = $uuid;
        return $row;
    }

    /**
     * Refresh the object, to populate createdAt and updatedAt if they are not already set, since they are generated in the database.
     */
    public function onDidSave(array $row) {
        if (isset($this->createdAt) && isset($this->updatedAt))return;

        $this->refresh();
    }

    /**
     * Set `$this->checked_will_delete = true`
     * @return true, always
     */
    public function onWillDelete(array $row): bool {
        $this->checked_will_delete = true;
        return true;
    }

    /**
     * Set `$this->is_deleted = true`
     */
    public function onDidDelete(array $row) {
        $this->is_deleted = true;
    }
}
