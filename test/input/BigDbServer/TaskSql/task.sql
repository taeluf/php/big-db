-- @query(get_incomplete_future_tasks)
SELECT `title` FROM `task` 
    WHERE `due_date` > CURRENT_TIMESTAMP 
        AND `date_completed` IS NOT NULL
;
