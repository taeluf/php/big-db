<?php

namespace Tlf\BigDb\Test;

class OrmTesterDb extends \Tlf\BigDb {

    protected string $orm_namespace = 'Tlf\\BigDb\\Test\\OrmTesterOrm';
    protected string $db_name = 'ormtester';
}
