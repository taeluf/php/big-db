DROP TABLE `person`;
CREATE TABLE `person` (
    `id` int PRIMARY KEY,
    `name` varchar(128),
    `date_of_birth` DATE,
    `created_at` DATETIME DEFAULT NOW() NOT NULL,
    `updated_at` DATETIME DEFAULT NOW() ON UPDATE CURRENT_TIMESTAMP NOT NULL
);
