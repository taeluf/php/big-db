<?php

namespace Tlf\BigDb\Test;

class OrmTester extends \Tlf\BigDb\OrmTester {


    protected \PDO $pdo;
    protected \Tlf\BigDb\Test\OrmTesterDb $db;
    public string $orm_class = '\\Tlf\\BigDb\\Test\\OrmTesterOrm';

    public array $valid_form_data = [
        
    ];

    /** Initial rows to setup before tests are run. All tables listed will be emptied before inserting initial data. */
    public array $create_before_test = [
        'person' =>[
            'columns'=> ['id', 'name', 'date_of_birth'],
            'rows'=> [
                [1, "Reed", "2000-01-01"],
                [2, "Bobbi", "1999-07-16"],
                [3, "Jeffrey", "1996-05-19"],
                [4, "Get out of here jim", "1037-11-27"],
            ]
        ],
    ];

    public function get_bigdb(\PDO $pdo): \Tlf\BigDb {
            if (isset($this->db)) return $this->db;

            $this->db = new \Tlf\BigDb\Test\OrmTesterDb($pdo);
            return $this->db;
    }

    public function prepare(){
        $this->pdo = $this->get_pdo();
        $this->get_bigdb($this->pdo);
        $this->db->migrate(0,1);

        parent::prepare();

    }




}
