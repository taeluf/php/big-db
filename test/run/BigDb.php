<?php

namespace Tlf\BigDb\Test;

class BigDb extends \Tlf\Tester {


    /** 
     *
     * Get array of tests. 
     *
     * prop_value is coerced & coerced prop_value must be equal to declared db_value. db_value is coerced & the coerced db_value must be equal to to the declared prop_value
     *
     * test_info is array w/ three keys: type, prop_value, db_value
     * test_info MAY contain key 'prop_name', which is used by some conversions like uuid.
     *
     * @return array<int index, array test_info>
     */
    protected function get_coersion_tests(): array {
        require_once($this->file("test/input/TestEnums.php"));

        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb($pdo);
        $orm = new \Tlf\BigOrm($db);

        return 
           [ 
                [ 'type'=> 'DateTime',
                  'prop_value'=> \DateTime::createFromFormat('Y-m-d H:i:s', '2023-11-10 04:52:13'), 
                  'db_value' => '2023-11-10 04:52:13'
                ],

                [ 'type'=> 'DateTime',
                  'prop_value'=> \DateTime::createFromFormat('Y-m-d H:i:sP', '2023-11-10 04:52:13-06:00'), 
                  'db_value' => '2023-11-10 10:52:13'
                ],

                [ 'type'=> 'string',
                  'prop_name' => 'uuid',
                  'prop_value' => 'df1a0f63-6937-11ee-befa-8c8caa96c56b',
                  'db_value' => $orm->uuid_to_bin('df1a0f63-6937-11ee-befa-8c8caa96c56b'),
                ],
                [ 'type'=>'Tlf\\BigDb\\Test\\TestEnums\\Suit',
                  'prop_value'=>\Tlf\BigDb\Test\TestEnums\Suit::Hearts,
                  'db_value' => \Tlf\BigDb\Test\TestEnums\Suit::Hearts->value,
                ]

            ];

    }

    public function prepare(){
        require_once($this->file('test/input/BigDbApp/ArticleDb.php'));
        require_once($this->file('test/input/BigDbApp/orm/Article.php'));
        require_once($this->file('test/input/BigDbApp/orm/Author.php'));
        require_once($this->file('test/input/BigDbApp/orm/Tag.php'));
    }

    /**
     * Run benchmark on coersions. 
     * Test that 50+ coersions are completed per ms
     */
    public function testBenchCoersion(){

        // Note: We previously tested for greater than 150 (one-hundred-fifty) coersions per ms.
        // It's slower now & I thiiink its because I have xdebug installed 
        // I think xdebug may have an effect even when I don't have it enabled ... but idk if I'm actually disabling it correctly...
        // And from profiling the code, it looks like the main hangup is with DateTime stuff, which I can't make faster. I tried.
        // So, whatever. We're testing for 50 coersions per ms now. That's still reasonably fast, i guess & it ensures there's not any HUGE slowdowns.

        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb($pdo);
        $orm = new \Tlf\BigOrm($db);




        $i = 0;
        //$loops = 1000 * 10;
        $loops = 100 * 10;

        $tests = $this->get_coersion_tests();
        // times 2 bc we coerce TO db & FROM db for each test
        $total_coersion_count = ( count($tests) * $loops * 2);

        $start_time = microtime(true);
        for ($i=0;$i<$loops;$i++) {
            //$this->status = \DecaturVote\News\Types\ArticleStatus::from($row['status']);
            //$this->type = \DecaturVote\News\Types\ArticleType::from($row['type']);

            foreach ($this->get_coersion_tests() as $index=>$test_info){
                $type = $test_info['type'];
                $prop_value = $test_info['prop_value'];
                $db_value = $test_info['db_value'];
                $prop_name = $test_info['prop_name'] ?? '';

                $coerced_prop_value = $db->coerce_from_db($type, $db_value, $prop_name);
                $coerced_db_value = $db->coerce_to_db($type, $prop_value, $prop_name);
                //$this->compare_raw($prop_value, $coerced_prop_value);
                //$this->compare($db_value, $coerced_db_value);
            }
        }

        $bench_results = $this->benchEnd($start_time);

        echo "\n\n----\n$total_coersion_count coersions were run. There were ".count($tests)." tests per loop, 2 coersions per test, and $loops loops";
        $total_time = $bench_results['diff'];
        $as_ms = $total_time * 1000;
        $time_per_coersion = $as_ms / $total_coersion_count;
        $time_for_100 = $time_per_coersion * 100;
        $coersions_in_1ms = $total_coersion_count / $as_ms;
        echo "\n";
        echo "\n {$as_ms}ms to run $total_coersion_count coersions";
        echo "\n It took {$time_per_coersion}ms per coersion";
        echo "\n It would take {$time_for_100}ms for 100 coersions";
        echo "\n {$coersions_in_1ms} coersions are completed in 1ms";

        echo "\n\nBench Results:\n";
        print_r($this->benchEnd($start_time));

        $this->test("Greater than 50 coersions per ms");
        $this->is_true($coersions_in_1ms > 50);

        echo "\n\nNote: We previously tested for 150 coersions per ms, but have lowered it. (see comments in the test method)";

    }

    /**
     * Test coercing property values into database-values & visa-versa
     */
    public function testCoerceValues(){

        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb($pdo);

        foreach ($this->get_coersion_tests() as $index=>$test_info){
            $type = $test_info['type'];
            $prop_value = $test_info['prop_value'];
            $db_value = $test_info['db_value'];
            $prop_name = $test_info['prop_name'] ?? '';

            $coerced_prop_value = $db->coerce_from_db($type, $db_value, $prop_name);
            $coerced_db_value = $db->coerce_to_db($type, $prop_value, $prop_name);
            $this->compare_raw($prop_value, $coerced_prop_value);
            $this->compare($db_value, $coerced_db_value);
        }
    }

    public function testQueryWithVar(){
        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb\Test\ArticlesDb($pdo);
        $db->recompile_sql();
        $db->migrate(0,1);
        
        $articles = $db->query('article', 'get_where_clause', ['where_clause'=>"WHERE `status` LIKE 'public'"]);

        $rows = [];
        foreach ($articles as $a){
            $rows[] = ['id'=>$a->id, 'title'=>$a->title];
        }

        $this->test("Binding to SELECT works");
        $this->compare_arrays(
            [   
                ['id'=>1, 'title'=>'One'],
                ['id'=>2, 'title'=>'Two'],
                ['id'=>3, 'title'=>'Three'],
            ],
            $rows
        );

    }

    public function testQueryWithBind(){
        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb\Test\ArticlesDb($pdo);
        $db->recompile_sql();
        $db->migrate(0,1);
        
        $articles = $db->query('article', 'get_where', ['status'=>'public']);

        $rows = [];
        foreach ($articles as $a){
            $rows[] = ['id'=>$a->id, 'title'=>$a->title];
        }

        $this->test("Binding to SELECT works");
        $this->compare_arrays(
            [   
                ['id'=>1, 'title'=>'One'],
                ['id'=>2, 'title'=>'Two'],
                ['id'=>3, 'title'=>'Three'],
            ],
            $rows
        );

        $rowCount = $db->exec('article.insert_with_status', ['status'=>'pumpkin']);
        $this->test("Binding to INSERT works");
        $this->is_int($rowCount);

        $dirty_rows = $db->select('article', ['title'=>'Status']);
        $rows = [];
        foreach ($dirty_rows as $a){
            $a = (object)$a;
            $rows[] = ['id'=>$a->id, 'title'=>$a->title, 'status'=>$a->status];
        }
        $this->compare_arrays(
            [
                ['id'=>5, 'title'=>'Status', 'status'=>'pumpkin'],
            ],
            $rows
        );

    }

    public function testQueryArticles(){
        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb\Test\ArticlesDb($pdo);
        $db->recompile_sql();
        $db->migrate(0,1);
        
        $articles = $db->query('article', 'get_public');

        $rows = [];
        foreach ($articles as $a){
            $rows[] = ['id'=>$a->id, 'title'=>$a->title];
        }

        $this->test("Use stored query to get public articles");
        $this->compare_arrays(
            [   ['id'=>1, 'title'=>'One'],
                ['id'=>2, 'title'=>'Two'],
                ['id'=>3, 'title'=>'Three'],
            ],
            $rows
        );


        $private_articles = $db->query('article', 'get_private');

        $rows = [];
        foreach ($private_articles as $a){
            $rows[] = ['id'=>$a->id, 'title'=>$a->title];
        }

        $this->test("Use stored query to get private articles");
        $this->compare_arrays(
            [   
                ['id'=>4, 'title'=>'Four, Private'],
            ],
            $rows
        );
    }

    public function testMain(){

        $pdo = $this->getPdo();
        $db = new \Tlf\BigDb\Test\ArticlesDb($pdo);
        $db->recompile_sql();
        $db->migrate(0,1);

        $articles = $db->get('article');

        $rows = [];
        foreach ($articles as $a){
            $rows[] = ['id'=>$a->id, 'title'=>$a->title];
        }

        $this->compare_arrays(
            [   ['id'=>1, 'title'=>'One'],
                ['id'=>2, 'title'=>'Two'],
                ['id'=>3, 'title'=>'Three'],
                ['id'=>4, 'title'=>'Four, Private'],
            ],
            $rows
        );


    }

}
