<?php

namespace Tlf\BigDb;

/**
 * Provides common verbs like insert, update, select, and delete. Returns array rows, not orms. 
 */
trait SqlVerbs {

    /**
     * Get array results. Uses standard PDO prepare/bind/execute/fetchAll(FETCH_ASSOC).
     *
     * @param $sql regular sql code
     * @param $binds an array of binds, as used by PDO.
     */
     public function sql_query(string $sql, array $binds = []): array{
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($binds);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    /**
     * Insert a row into the database.
     *
     * @param $table the sql table name
     * @param $row array<string, mixed> a row to insert with @key=db column  & @value=db value
     * @return newly inserted id
     */
    public function insert(string $table, array $row): int {
        return $this->ldb->insert($table, $row);
    }

    /**
     * Update the database. Can affect multiple rows.
     *
     * @param $table table name
     * @param $where array<string, mixed> WHERE clauses, where dbcolumn (@key) = db value (@value)
     * @param $new_values array<string, mixed> the new values to set where the @key is the db column & @value is the db value.
     * @return number of rows affected
     */
    public function update(string $table, array $where, array $new_values): int {
        return $this->ldb->updateWhere($table, $new_values, $where);
    }

    /**
     * Get an array of rows for the given where clauses clauses.
     *
     * @param $table string table name
     * @param $where array<string, mixed> of where-clauses. Each key=>value pair is a `WHERE/AND key = value`
     *
     * @return an array of rows, as returned by the database
     */
    public function select(string $table, array $where): array {
        return $this->ldb->select($table, $where);
    }

    /**
     * Delete rows from a table
     *
     * @param $table string table name
     * @param $where array<string, mixed> of where-clauses. Each key=>value pair is a `WHERE/AND key = value`
     *
     * @return true if any rows were deleted. false otherwise
     */
    public function delete(string $table, array $where) : bool {
        return $this->ldb->delete($table, $where);
    }
}
