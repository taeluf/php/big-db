<?php

namespace Tlf;

/**
 * Extend this class (instead of BigOrm) to handle user-supplied form data. This extends from BigOrm & provides additional convenience methods & a hook for setting ORM data from form.
 *
 */
class BigFormOrm extends BigOrm {

    /**
     * Sanitize user input by removing all html if the input is non-numeric
     *
     * @param $input unsafe user input
     * @return input if it `is_numeric()` or `strip_tags()` input otherwise. 
     * @throws if an array or object is passed.
     */
    public function sanitize(mixed $input): mixed{
        if (is_numeric($input))return $input;
        else if (is_array($input)||is_object($input))throw new \RuntimeException("Arrays and objects cannot be passed to sanitize()");

        return strip_tags($input);
    }

    /**
     * Turn a string into a slug (for URLs. No slimey critters involved.)
     *
     * @param $text
     * @return a string slug, all lowercase, only alpha-numeric characters and hyphens
     */
    public function slugify(string $text): string{
        $slug = preg_replace('/[^0-9a-zA-Z\-\ ]/','',$text);
        $slug = strtolower($slug);
        $slug = str_replace(' ','-',$slug);
        $slug = str_replace(['---','--'],'-',$slug);
        return $slug;
    }

    /**
     * Initialize the Orm object from a user-submitted form
     *
     * @param $data array<string, mixed> user submitted data, typically same as `$_POST`
     * @param $form_id an optional form identifier.
     * @return void
     *
     * @override required because there is no default implementation
     */
    public function set_from_form(array $data, mixed $form_id = null){
        throw new \RuntimeException("You must override `set_from_form(array \$row)` in your ORM class '".get_class($this)."'");
    }

}
