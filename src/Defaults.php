<?php

namespace Tlf\BigDb;

/**
 * Provides default configuration constants
 */
class Defaults {

    const CONFIG_FILE_LOCATION = "config/bigdb.json";
    const APP_CLASS = "\Tlf\BigDb";

} 
